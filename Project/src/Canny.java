import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

//
// Our own implementation of canny edge detection
//
// It basically does 5 operations:
// 1. perform gaussian blur (we followed the class workshop to perform 2x1D gaussian blur with sigma = 2
// 2. find out sobel gradients of each point and its direction
// 3. perform non-maximal suppression
// 4. threshold the result with a high threshold value
// 5. perform hysteresis to restore weaker edges above the low threshold value

public class Canny {

	final static double HIGHTRESHOLDSD = 0.5;
	final static double LOWTHRESHOLDSD = 0.5;
			
	int width;
	int height;
	byte srcImg[];
	int direction[];
	boolean processed[];
	double maxMagnitude;
	double magnitude[];
	double suppressed[];
	double xGrad[];
	double yGrad[];
	DescriptiveStatistics stats;
	
	public Canny(byte[]img, int width, int height)
	{
		srcImg = img;
		this.width = width;
		this.height = height;
		xGrad = new double[img.length];
		yGrad = new double[img.length];
		magnitude = new double[img.length];
		direction = new int[img.length];
		maxMagnitude = -1;
		stats = new DescriptiveStatistics();
	}
	
	// simulate dot product of 2 vectors in 8 directions only
	private double getDotProduct(int myDir, int loc)
	{
		int otherDir = direction[loc];
		double factor = 1;
		int diff = Math.abs(myDir - otherDir);
		switch(diff)
		{
		case 0:
		case 8:
			factor = 1;
			break;
		case 1:
		case 7:
			factor = 0.7;
			break;
		case 2:
		case 6:
			factor = 0;
			break;
		case 3:
		case 5:
			factor = -0.7;
			break;
		case 4:
			factor = -1;
			break;
		default:
			System.out.println("Unexpected product of " + myDir + " & " + otherDir);
			break;
		}
		return(magnitude[loc]*factor);
	}

	// simulate dot products of 16 directions
	private double getDotProduct16(int myDir, int loc)
	{
		int otherDir = direction[loc];
		double factor = 1;
		int diff = Math.abs(myDir - otherDir);
		switch(diff)
		{
		case 0:
		case 16:
			factor = 1;
			break;
		case 1:
		case 15:
			factor = 0.92;
			break;
		case 2:
		case 14:
			factor = 0.7;
			break;
		case 3:
		case 13:
			factor = 0.38;
			break;
		case 4:
		case 12:
			factor = 0;
			break;
		case 5:
		case 11:
			factor = -0.38;
			break;
		case 6:
		case 10:
			factor = -0.7;
			break;
		case 7:
		case 9:
			factor = -0.38;
			break;
		case 8:
			factor = -1;
			break;
		default:
			System.out.println("Unexpected product of " + myDir + " & " + otherDir);
			break;
		}
		return(magnitude[loc]*factor);
	}
	
	// perform non-maximal suppression of gradients of 8 directions
	// according to the directions of the gradient compare the corresponding 8 neighbor pixels
	public void nonMaxSuppression()
	{
		suppressed = new double[magnitude.length];

		for(int x=1;x<height-1;x++)
		{
			for(int y=1;y<width-1;y++)
			{
				int loc = x*width+y;
				double curmag = magnitude[loc];
				int myDir = direction[loc];
				suppressed[loc] = 0;
				switch(direction[loc])
				{
					case 0:
					case 8:
						if(    curmag >= getDotProduct(myDir, loc+width)
							&& curmag >  getDotProduct(myDir, loc-width))
							suppressed[loc]= curmag;
					break;
					case 1:
						if(    curmag >= getDotProduct(myDir, loc+width+1)
							&& curmag >  getDotProduct(myDir, loc-width-1))
							suppressed[loc]= curmag;
					break;
					case 2:
						if(    curmag >= getDotProduct(myDir, loc+1)
							&& curmag >  getDotProduct(myDir, loc-1))
							suppressed[loc]= curmag;
					break;

					case 3:
						if(    curmag >= getDotProduct(myDir, loc-width+1)
							&& curmag >  getDotProduct(myDir, loc+width-1))
							suppressed[loc]= curmag;
					break;
					case 4:
						if(    curmag >= getDotProduct(myDir, loc-width)
							&& curmag >  getDotProduct(myDir, loc+width))
							suppressed[loc]= curmag;
					break;
					case 5:
						if(    curmag >= getDotProduct(myDir, loc-width-1)
							&& curmag >  getDotProduct(myDir, loc+width+1))
							suppressed[loc]= curmag;
					break;
					case 6:
						if(    curmag >= getDotProduct(myDir, loc-1)
							&& curmag >  getDotProduct(myDir, loc+1))
							suppressed[loc]= curmag;
					break;
					case 7:
						if(    curmag >= getDotProduct(myDir, loc+width-1)
							&& curmag >  getDotProduct(myDir, loc-width+1))
							suppressed[loc]= curmag;
					break;
					
				}
			}
		}
	}
	
	// perform non-maximal suppression of gradients of 16 directions
	// for the n, w, s, e, ne, nw, se, sw directions, we compare the corresponding neighbor pixels
	// for the nnw, nne, ene, ese, sse, ssw, wsw, wnw directions, they are compared with the two
	// pixels adjacent to them. e.g. for a gradient of nne directions, it is compared with both n & ne
	// pixels
	// we found out that this method gives a cleaner edge diagram compare with 8, and at the same time
	// remain fast
	public void nonMaxSuppression16()
	{
		suppressed = new double[magnitude.length];

		for(int x=1;x<height-1;x++)
		{
			for(int y=1;y<width-1;y++)
			{
				int loc = x*width+y;
				double curmag = magnitude[loc];
				int myDir = direction[loc];
				suppressed[loc] = 0;
				switch(direction[loc])
				{
					case 0:
					case 16:
						if(    curmag >= getDotProduct16(myDir, loc+width)
							&& curmag >  getDotProduct16(myDir, loc-width))
							suppressed[loc]= curmag;
						break;
						
					case 1:
						if(    curmag >= getDotProduct16(myDir, loc+width)
							&& curmag >  getDotProduct16(myDir, loc-width)
							&& curmag >= getDotProduct16(myDir, loc+width+1)
							&& curmag >  getDotProduct16(myDir, loc-width-1))
							suppressed[loc]= curmag;
						break;
					
					case 2:
						if(    curmag >= getDotProduct16(myDir, loc+width+1)
							&& curmag >  getDotProduct16(myDir, loc-width-1))
							suppressed[loc]= curmag;
						break;
					
					case 3:
						if(    curmag >= getDotProduct16(myDir, loc+width+1)
							&& curmag >  getDotProduct16(myDir, loc-width-1)
							&& curmag >= getDotProduct16(myDir, loc+1)
							&& curmag >  getDotProduct16(myDir, loc-1))
							suppressed[loc]= curmag;
						break;

					case 4:
						if(	   curmag >= getDotProduct16(myDir, loc+1)
							&& curmag >  getDotProduct16(myDir, loc-1))
							suppressed[loc]= curmag;
						break;
						
					case 5:
						if(	   curmag >= getDotProduct16(myDir, loc+1)
							&& curmag >  getDotProduct16(myDir, loc-1)
							&& curmag >= getDotProduct16(myDir, loc-width+1)
							&& curmag >  getDotProduct16(myDir, loc+width-1))
							suppressed[loc]= curmag;
						break;

					case 6:
						if(    curmag >= getDotProduct16(myDir, loc-width+1)
							&& curmag >  getDotProduct16(myDir, loc+width-1))
							suppressed[loc]= curmag;
						break;

					case 7:
						if(    curmag >= getDotProduct16(myDir, loc-width+1)
							&& curmag >  getDotProduct16(myDir, loc+width-1)
							&& curmag >= getDotProduct16(myDir, loc-width)
							&& curmag >  getDotProduct16(myDir, loc+width))
							suppressed[loc]= curmag;
						break;
					
					case 8:
						if(    curmag >= getDotProduct16(myDir, loc-width)
							&& curmag >  getDotProduct16(myDir, loc+width))
							suppressed[loc]= curmag;
						break;

					case 9:
						if(    curmag >= getDotProduct16(myDir, loc-width)
							&& curmag >  getDotProduct16(myDir, loc+width)
							&& curmag >= getDotProduct16(myDir, loc-width-1)
							&& curmag >  getDotProduct16(myDir, loc+width+1))
							suppressed[loc]= curmag;
						break;
					
					case 10:
						if(    curmag >= getDotProduct16(myDir, loc-width-1)
							&& curmag >  getDotProduct16(myDir, loc+width+1))
							suppressed[loc]= curmag;
						break;
						
					case 11:
						if(    curmag >= getDotProduct16(myDir, loc-width-1)
							&& curmag >  getDotProduct16(myDir, loc+width+1)
							&& curmag >= getDotProduct16(myDir, loc-1)
							&& curmag >  getDotProduct16(myDir, loc+1))
							suppressed[loc]= curmag;
						break;
					
					case 12:
						if(    curmag >= getDotProduct16(myDir, loc-1)
							&& curmag >  getDotProduct16(myDir, loc+1))
							suppressed[loc]= curmag;
						break;

					case 13:
						if(    curmag >= getDotProduct16(myDir, loc-1)
							&& curmag >  getDotProduct16(myDir, loc+1)
							&& curmag >= getDotProduct16(myDir, loc+width-1)
							&& curmag >  getDotProduct16(myDir, loc-width+1))
							suppressed[loc]= curmag;
						break;
						
					case 14:
						if(    curmag >= getDotProduct16(myDir, loc+width-1)
							&& curmag >  getDotProduct16(myDir, loc-width+1))
							suppressed[loc]= curmag;
						break;

					case 15:
						if(    curmag >= getDotProduct16(myDir, loc+width-1)
							&& curmag >  getDotProduct16(myDir, loc-width+1)
							&& curmag >= getDotProduct16(myDir, loc+width)
							&& curmag >  getDotProduct16(myDir, loc-width))
							suppressed[loc]= curmag;
					break;
					
				}
			}
		}
	}
	
	// the canny edge detection process itself
	public void process()
	{
		gradient();
		nonMaxSuppression16();
		threshold();
		hystersis();
		
	}
	
	// finding sobel gradient of each pixel
	public void gradient()
	{
		Project imgprocessor = new Project();
		byte img[] = Arrays.copyOf(srcImg, srcImg.length);
		imgprocessor.gaussianSmoothing(img, 2, width, height);
		for(int x=1;x<height-1;x++)	{
			for(int y=1;y<width-1;y++)	{
				int loc = x*width+y;
				double fx =    (img[(x+1)*width+y-1] & 0xFF) - (img[(x-1)*width+y-1] & 0xFF)
							 + 2*(img[(x+1)*width+y] & 0xFF) - 2*(img[(x-1)*width+y] & 0xFF)
							 + (img[(x+1)*width+y+1] & 0xFF) - (img[(x-1)*width+y+1] & 0xFF);
				double fy =    (img[(x-1)*width+y+1] & 0xFF) - (img[(x-1)*width+y-1] & 0xFF)
						 	 + 2*(img[x*width+y+1] & 0xFF) - 2*(img[x*width+y-1] & 0xFF)
						 	 + (img[(x+1)*width+y+1] & 0xFF) - (img[(x+1)*width+y-1] & 0xFF);				
				xGrad[loc] = fx;
				yGrad[loc] = fy; 
				magnitude[loc] = Math.sqrt(fx*fx + fy*fy);
				// collect stats of the gradient
				stats.addValue(magnitude[loc]);
				
				double theta = Math.atan2(fy,fx);
				if ( fy < 0 )
					theta += 2*Math.PI;

				//direction[loc] = (int) Math.round((4*theta/Math.PI - Math.PI/8));
				// classified the gradient in one of the 16 direction, s = 0, sse=1, se=2, etc
				direction[loc] = (int) Math.round((8*theta/Math.PI - Math.PI/16));
				if(magnitude[loc] > maxMagnitude)
					maxMagnitude = magnitude[loc];
			}
		}
		System.out.println("Max magnitude: "+ stats.getMax());
		System.out.println("Mean: "+ stats.getMean());
		System.out.println("SD: "+ stats.getStandardDeviation());
	}
	
	// trace the edges to restore the weaker edges
	// using a queue to store the pixels without the need of recurisve call
	private void traceLine(int startx, int starty, double hithreshold, double lowthreshold)
	{
		int[] curPoint = {startx, starty};
		int[] newPoint;
		Queue<int[]>toBeTraced = new LinkedList<int[]>();
		toBeTraced.add(curPoint);
		processed[curPoint[0]*width + curPoint[1]] = true;
		while( toBeTraced.size() > 0 )
		{
			curPoint = toBeTraced.remove();
			if( suppressed[curPoint[0]*width + curPoint[1]] > lowthreshold)
			{
				magnitude[curPoint[0]*width + curPoint[1]] = hithreshold;
			}
			for(int i=-1; i<2;i++)
			{
				for(int j=-1;j<2;j++)
				{
					if(i==0 && j==0) continue;
					if( curPoint[0]+i < 0 || curPoint[0] + i >= height ) continue;
					if( curPoint[1]+j < 0 || curPoint[1] + j >= width ) continue;
					if( suppressed[(curPoint[0]+i)*width+curPoint[1]+j] > lowthreshold )
					{
						if(!processed[(curPoint[0]+i)*width + curPoint[1]+j])
						{
							processed[(curPoint[0]+i)*width + curPoint[1]+j] = true;
							newPoint = new int[2];
							newPoint[0] = curPoint[0]+i;
							newPoint[1] = curPoint[1]+j;
							toBeTraced.add(newPoint);
						}
					}
				}
			}
		}
	}
	
	// performing threshold of the edges with the high threshold value 
	public void threshold()
	{
		threshold(HIGHTRESHOLDSD);
	}
	
	// performing threshold of the edges with the high threshold value in unit of
	// mean + SD 
	public void threshold(double numSD)
	{
		double hithreshold = stats.getMean() + numSD * stats.getStandardDeviation();
		System.out.println("High threshold: " + hithreshold);
		for(int x=0;x<height;x++)
		{
			for(int y=0;y<width;y++)
			{
				if(suppressed[x*width+y] > hithreshold)
					magnitude[x*width+y] = maxMagnitude;
				else
					magnitude[x*width+y] = 0;
			}
		}
	}
	
	// perform hysteresis
	
	public void hystersis()
	{
		// low threshold is the bigger value of mean/2 or mean - 0.5 SD
		double lowthreshold = Math.max(stats.getMean()/2, stats.getMean() - LOWTHRESHOLDSD *stats.getStandardDeviation());
		System.out.println("Low threshold: " + lowthreshold);
		processed = new boolean[magnitude.length];

		for(int x=0;x<height;x++)
		{
			for(int y=0;y<width;y++)
			{
				if(magnitude[x*width+y] >= maxMagnitude && !processed[x*width+y])
					traceLine(x, y, maxMagnitude, lowthreshold);
			}
		}
				
	}
	
	// get graphical display of the magnitude of the gradient value
	public BufferedImage getMagnitude()
	{
		BufferedImage b = new BufferedImage( width, height, BufferedImage.TYPE_BYTE_GRAY);
		byte[] imgData = ((DataBufferByte)b.getRaster().getDataBuffer()).getData();
		for(int i=0;i<magnitude.length;i++)		
		{
			imgData[i]= (byte)Math.min((int)((magnitude[i]/maxMagnitude) * 255), 255);
		}
		return b;
	}

	// get an array of the gradient magnitude value	
	public byte[] getMagnitudeArray()
	{
		byte[] imgData = new byte[magnitude.length];
		for(int i=0;i<magnitude.length;i++)		
		{
			imgData[i]= (byte)Math.min((int)((magnitude[i]/maxMagnitude) * 255), 255);
		}
		return imgData;
	}
	
	// get a color image showing the gradient of magnitude and the direction
	public BufferedImage getGradient(boolean gray)
	{
		BufferedImage b = new BufferedImage( width, height, gray?BufferedImage.TYPE_BYTE_GRAY:BufferedImage.TYPE_INT_BGR);
		byte[] imgData = null;
		if( gray )
			imgData = ((DataBufferByte)b.getRaster().getDataBuffer()).getData();
		double[] mag = magnitude;
		for(int i=0;i<direction.length;i++)		
		{
			int intensity = Math.min((int)((mag[i]/maxMagnitude) * 255), 255);
			int color = 0;

			if( gray ) imgData[i] = (byte)intensity;
			else
			{
				switch (direction[i])
				{
				case 0:
				case 1:
				case 2:
				case 14:
				case 15:
				case 16:
					color |=  intensity << 16;
					break;
				
				case 3:
					color |=  intensity << 16 | (intensity/2) << 8;
					break;
				
				case 4:
				case 5:
				case 6:
				case 7:
					color |=  intensity << 8;
					break;
					
				case 8:
					color |= (intensity/2) << 8 | (intensity/2);
					break;
					
				case 9:
				case 10:
				case 11:
				case 12:
					color |= intensity;
					break;
					
				case 13:
					color |= (intensity/2) << 16 | (intensity/2);
					break;
				}
				
				b.setRGB(i%width, i/width,  color);
			}

		}
		return b;
	}
	
	// get the image of edges after non-maximal suppression
	public BufferedImage getSuppressed()
	{
		BufferedImage b = new BufferedImage( width, height, BufferedImage.TYPE_BYTE_GRAY);
		byte[] imgData = ((DataBufferByte)b.getRaster().getDataBuffer()).getData();
		for(int i=0;i<suppressed.length;i++)		
		{
			imgData[i]= (byte)Math.min((int)((suppressed[i]/maxMagnitude) * 255), 255);
		}
		return b;
		
	}
}
