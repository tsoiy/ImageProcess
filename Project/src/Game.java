
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kinyipchan on 2/11/15.
 */

// model the othello game

public class Game {

    //
    public Boolean gameOver = false;
    public Boolean showHints = false;
    public int turn = 0;
    public String message = "";

    //-----------
    // 0 : none
    // 1 : black
    // 2 : white
    //-----------
    public final int NULL = 0;
    public final int BLACK = -1;
    public final int WHITE = 1;

    //
    private int row = 0;
    private int col = 0;
    private int boardSize = 0;
    private Map<Integer, Map<Integer, Integer>> boardData;

    public void initBoardData(){
        if(boardSize == 0){
            boardSize = 8;
        }

        boardData = new HashMap<Integer, Map<Integer, Integer>>();
        for(int r=0; r<boardSize; r++){
            Map<Integer, Integer> colData = new HashMap<Integer, Integer>();
            for(int c=0; c<boardSize; c++){
                colData.put(c, NULL);
            }
            boardData.put(r, colData);
        }
    }

    public void setBoardSize(int size){
        this.boardSize = size;
    }

    public int getBoardSize(){
        return this.boardSize;
    }

    public int getBoardData(int row, int col){
        return this.boardData.get(row).get(col);
    }

    public void setBoardData(int row, int col, int value){
        Map<Integer, Integer> colData = boardData.get(row);
        colData.put(col, value);
        boardData.put(row, colData);
    }

    public void setMove(int row, int col){
        this.row = row;
        this.col = col;
    }

    public void resetMove(){
        this.row = -1;
        this.col = -1;
    }

    public void setMove(int userCode, int r, int c){
        //set move on all direction
        for(int x=-1; x<=1; x++){
            for(int y=-1; y<=1; y++){
                if(x==0 && y==0){
                    this.setBoardData(r, c, userCode);
                }else{
                    if (isPossibleMoveDirection(userCode, r, c, x, y)) {
                        setMoveDirection(userCode, r, c, x, y);
                    }
                }
            }
        }
    }

    public void setMoveDirection(int userCode, int r, int c, int x, int y) {

        int len = 1;

        while(true) {
            int xl = r + x * len;
            int yl = c + y * len;

            if (xl >= 0 && yl >= 0 && xl < boardSize && yl < boardSize) {

                int cell = getBoardData(xl, yl);
                if (cell != NULL && cell != userCode) {
                    setBoardData(xl, yl, userCode);
                    len++;
                    continue;
                }
            }
            break;
        }
    }

    public int possibleMoveCount(int userCode){

        int possibleMove = 0;
        for(int r=0; r<boardSize; r++){
            for(int c=0; c<boardSize; c++){
                if(getBoardData(r, c) == NULL){
                    if(isPossibleMove(userCode, r, c)) {
                        possibleMove++;
                        //Log.d("test", "pos:"+r+","+c);
                    }
                }
            }
        }

        return possibleMove;
    }

    public Boolean isPossibleMove(int userCode, int r, int c){

        //check different direction
        for(int x=-1; x<=1; x++){
            for(int y=-1; y<=1; y++){
                if(!(x==0 && y==0)){
                	if (getBoardData(r,c) != 0)
                		return false;
                    if (isPossibleMoveDirection(userCode, r, c, x, y)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public Boolean isPossibleMoveDirection(int userCode, int r, int c, int x, int y) {

        int len = 1;

        while(true) {
            int xl = r + x * len;
            int yl = c + y * len;

            if (xl >= 0 && yl >= 0 && xl < boardSize && yl < boardSize) {

                int cell = getBoardData(xl, yl);
                if (cell != NULL || cell != 0) {
                    if (cell != userCode) {
                        //extend the direction check
                        len++;
                        continue;
                    } else if (cell == userCode && len > 1) {
                        //
                        return true;
                    }
                }
            }

            break;
        }

        return false;
    }
    
    public String toString()
    {
    	StringBuilder sb = new StringBuilder();
		sb.append("+===============+\n");
    	for(int i = 0;i<8;i++)
    	{
    		sb.append("|");
    		for(int j=0;j<8;j++)
    		{
    			switch(getBoardData(i,j))
    			{
    			case(0):
    				sb.append(" ");
    				break;
    			case(1):
    				sb.append("O");
    				break;
    			case(-1):
    				sb.append("X");
    				break;
    			}
    			sb.append("|");
    		}
    		sb.append("\n");
    	}
    	sb.append("+===============+\n");
		return (sb.toString());
    }

}
