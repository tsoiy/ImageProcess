import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;

// UI for capture image from web cam

public class CaptureUI extends JDialog implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	BufferedImage dest;
	Webcam webcam;
	public CaptureUI(Frame owner, String title)
	{
		super(owner, title, true);
		dest = null;
        setLocation(new Point(owner.getLocation().x+100, owner.getLocation().y+100));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        
		webcam = Webcam.getDefault();
		webcam.setCustomViewSizes(new Dimension[]{new Dimension(1024, 768)});
		webcam.setViewSize(new Dimension(1024, 768));
		WebcamPanel panel = new WebcamPanel(webcam);
		panel.setFPSDisplayed(true);
		panel.setDisplayDebugInfo(true);
		panel.setImageSizeDisplayed(true);
		panel.setMirrored(false);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		JButton capButton= new JButton("Capture");
        capButton.setActionCommand("capture");
        capButton.addActionListener(this);
        buttonPanel.add(capButton, BorderLayout.WEST);
        
		JButton cancelButton= new JButton("Cancel");
        cancelButton.setActionCommand("cancel");
        cancelButton.addActionListener(this);
        buttonPanel.add(cancelButton, BorderLayout.EAST);
		
		getContentPane().add(panel, BorderLayout.NORTH);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		pack();

	}
	public BufferedImage showDialog()
	{
		setVisible(true);
		return dest;
	}
		

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "capture") {
			dest = webcam.getImage();
			webcam.close();
			setVisible(false);
		}
		else if (e.getActionCommand() == "cancel") {
			dest = null;
			webcam.close();
			setVisible(false);
		}
		
	}
}
