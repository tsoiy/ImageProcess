import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//
//Our own implementation of hough's transform circle detection of known radius
//
//Each pixel of the supplied image is rendered with a series of circles in 360 degrees, all
//with the required radius, each of them passing through the pixel

public class HoughCircle {

	final static int STEPS = 360;
	final static double THRESHOLD = 0.5;
	int width;
	int height;
	double sinTable[];
	double cosTable[];
	int houghTable[][];
	int maxVote;
	double radius;
	
	
	public HoughCircle(int width, int height)
	{
		this.width = width;
		this.height = height;
		sinTable = new double[STEPS];
		cosTable = new double[STEPS];
		houghTable = new int[width][height];
		maxVote = 0;
		for(int i=0;i<STEPS;i++)
		{
			sinTable[i] = Math.sin(2.0*i/STEPS * Math.PI);
			cosTable[i] = Math.cos(2.0*i/STEPS * Math.PI);
		}
	}
	
	// process each pixels and plotted the circles and accumulate the votes
	public void transform(byte[] f, double r)
	{
		radius = r;
		for(int y=0;y<height;y++)
		{
			for(int x=0;x<width;x++)
			{
				if( (int)(f[y*width+x] & 0xFF) == 255 )
				{
					for(int i = 0; i < STEPS; i++)
					{
						int a = (int)(x + r * sinTable[i]);
						int b = (int)(y + r * cosTable[i]);
						if( a >= 0 && a < width && b >= 0 && b < height )
						{
							houghTable[a][b] ++;
							if( houghTable[a][b] > maxVote)
								maxVote = houghTable[a][b];
						}
					}
				}
			}
		}
		
	}
	
	// show the hough graph of the votes
	public BufferedImage getHoughGraph()
	{
		BufferedImage b = new BufferedImage( width, height, BufferedImage.TYPE_BYTE_GRAY);
		byte[] imgData = ((DataBufferByte)b.getRaster().getDataBuffer()).getData();
		for(int y = 0;y<height; y++)
		{
			for(int x = 0; x < width; x++ )
			{
				imgData[y*width+x] = (byte)(255.0 * houghTable[x][y]/ maxVote);
			}
		}
		return b;
	}
	
	// extract the circles from the graph
	public List<Point2D> getCircles()
	{
		// only extract circles exceeding 0.5 of the max votes
		int threshold = (int)(maxVote * THRESHOLD);
		int sumX;
		int sumY;
		int count;
		int x2;
		int y2;
		int searchArea = (int)(radius/2);
		int temp[][] = new int[houghTable.length][houghTable[0].length];
		
		System.out.println("Hough circle max vote: " + maxVote);
		for(int r = 0; r < houghTable.length; r++)
		{
			temp[r] = Arrays.copyOf(houghTable[r], houghTable[0].length);
		}
		List<Point2D> circles = new ArrayList<Point2D>();
		for(int y = 0;y<height; y++)
		{
			for(int x = 0; x < width; x++ )
			{
				// found a potential center of circle
				if(temp[x][y] > threshold)
				{
					sumX = 0;
					sumY = 0;
					count = 0;
					
					// since the accumulator and the edges extracted may not be perfect
					// circular, the intersections of the circles (i.e. the real circle center)
					// may not all coincide at exactly one pixel. We search an area for all the
					// pixels that exceed the threshold and decide which pixel is the center
					
					// look at pixels within the square of radius/2 x radius/2
					// with the potential circle center pixel at the center
					for(int i = -searchArea;i<searchArea;i++)
					{
						for(int j = -searchArea;j<searchArea;j++)
						{
							y2 = i+y;
							x2 = j+x;
							if(x2 < 0 || x2 >= width || y2 < 0 || y2 >= height)
								continue;
							// record all the coordinates of pixels that exceeds threshold
							if(temp[x2][y2]> threshold)
							{
								sumX += x2;
								sumY += y2;
								count++;
								temp[x2][y2] = 0;
							}
						}
					}
					// use the simple average of these pixels as the center of circle
					// could use weighted average for better accuracy if needed.
					circles.add( new Point2D.Double(sumX/count, sumY/count));
				}
			}
		}
		return circles;
	}
}
