import java.util.Vector;

// our implementation of hough transform line detection

public class Hough {

	final static int STEPS = 360;	// no. of steps to divide the theta angle
	final static int CHECKSIZE = 5; // for non-maxmial suppression
	int width;
	int height;
	int rOffset;
	int maxVote;
	double sinTable[];
	double cosTable[];
	int houghTable[][];
	
	public Hough(int width, int height)
	{
		double stepsize = Math.PI / STEPS;

		
		// max possible R value
		rOffset = (int)(Math.sqrt(width*width+height*height)/2) + 1;
		this.width = width;
		this.height = height;
		sinTable = new double[STEPS];
		cosTable = new double[STEPS];
		houghTable = new int[2 * rOffset][STEPS];
		maxVote = 0;
		for(int i=0;i<STEPS;i++)
		{
			sinTable[i] = Math.sin(stepsize * i);
			cosTable[i] = Math.cos(stepsize * i);
		}
	}
	
	
	// apply hough transform on the pixels and accumulate the votes
	public void transform(byte[] f)
	{
		// Choose center of the image as origin
		int centerX = width/2;
		int centerY = height/2;
		
		for(int y=0;y<height;y++)
		{
			for(int x=0;x<width;x++)
			{
				if( (int)(f[y*width+x] & 0xFF)==255 )
				{
					for(int t=0;t<STEPS;t++)
					{
						int r = (int)((x - centerX)*cosTable[t] + (y-centerY)*sinTable[t]) + rOffset;
						if(r < 0 || r >= 2 * rOffset)
							continue;
						houghTable[r][t]++;
						if( houghTable[r][t] > maxVote)
							maxVote = houghTable[r][t];
					}
				}
			}
		}
	}
	
	// extract the lines
	public Vector<HoughLine> getLines(double threshold)
	{
		Vector<HoughLine>lines = new Vector<HoughLine>();
		int voteCount = (int)(maxVote * threshold);
		for(int r = 0;r<2*rOffset; r++)
			for(int t=0; t<STEPS; t++)
			{
				int curCount = houghTable[r][t];
				if ( curCount >= voteCount)
				{
					// search within the area for non-maximal suppression
					boolean localMaximum = true;
					for(int rx = - CHECKSIZE; rx <= CHECKSIZE && localMaximum; rx++ )
					{
						for( int tx = -CHECKSIZE; tx <= CHECKSIZE && localMaximum; tx++ )
						{
							int checkr = rx+r;
							int checkt = tx+t;
							if (checkr == r && checkt == t) continue;
							if (checkr < 0 || checkr >= 2*rOffset) continue;
							if (checkt < 0) checkt += STEPS;
							if (checkt >= STEPS) checkt -= STEPS;
							if (houghTable[checkr][checkt] > curCount || 
								(houghTable[checkr][checkt] == curCount && checkr >= r && checkt >= t))
								localMaximum = false;
						}
					}
					if( localMaximum )
						lines.add(new HoughLine(r - rOffset, t*Math.PI/STEPS, width, height, houghTable[r][t]));
				}
			}
		return lines;
	}
}
