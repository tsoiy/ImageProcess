import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

// class for handling a hough line

public class HoughLine implements Comparable<HoughLine>{ 
 
	int centerX;
	int centerY;
	int height;
	int width;
	int r; 
	double t; 
	int vote;
 

    public HoughLine(int r, double t, int width, int height, int vote) { 
        this.r = r;
    	this.t = t;
    	this.height = height;
    	this.width = width;
    	this.centerX = width/2;
    	this.centerY = height/2;
        this.vote = vote;
    } 
 
    public String toString(){
    	return ( "r: " + Double.toString(this.r) + " angle: " + Double.toString(this.t) + " x,y: (" + centerX + "," + centerY + ") + vote: " + vote);
    }
    
    private Double getX(double y)
    {
        double sint = Math.sin(t); 
        double cost = Math.cos(t);
        if (cost == 0)
        	return null;
        else
        	return (new Double(r - (y  * sint)) / cost);
    }

    private Double getY(double x)
    {
        double sint = Math.sin(t); 
        double cost = Math.cos(t);
        if (sint == 0)
        	return null;
        else
            return new Double((r - (x * cost)) / sint); 
    }
    
    // convert hough line back to a Line2D in orignial x-y coordinate system
    public Line2D toLine2D()
    {
 
        Point2D[] points = new Point2D[2];
        do
        {
        	int i =0;
        	Double value;
        	value = getX(-centerY);
        	if( value != null && value >= -centerX && value < centerX )
        	{
        		points[i] = new Point2D.Double(value + centerX, 0);
        		i++;
        	}
        	
        	value = getX(centerY-1);
        	if( value != null && value >= -centerX && value < centerX )
        	{
        		points[i] = new Point2D.Double(value + centerX, height);
        		i++;
        	}
        	if(i>=2) break;
        	
        	value = getY(-centerX);
        	if( value != null && value >= -centerY && value < centerY )
        	{
        		points[i] = new Point2D.Double(0, value + centerY);
        		i++;
        	}
        	if(i>=2) break;
        	
        	value = getY(centerX-1);
        	if( value != null && value >= -centerY && value < centerY )
        	{
        		points[i] = new Point2D.Double(width, value + centerY);
        		i++;
        	}
        }while (false);
        return( new Line2D.Double(points[0], points[1]));
    }
    
    
    // find the intersection points of Hough lines
    static public Point2D getLineIntersection(HoughLine l1, HoughLine l2)
    {
    	double sint1 = Math.sin(l1.t);
    	double sint2 = Math.sin(l2.t);
    	double cost1 = Math.cos(l1.t);
    	double cost2 = Math.cos(l2.t);
    	double y;
    	double x;
    	// parallel
    	if( sint1 == sint2 && cost1 == cost2 ) return null;

    	// horizontal line
    	if( cost1 == 0 || sint1 == 1.0 ) {
    		y = l1.r;
        	x = l2.getX(y);
    	}
    	else if ( cost2 == 0 || sint2 == 1.0 ) {
    		y = l2.r;
    		x = l1.getX(y);
    	}
    	else {
    		y = (l1.r*cost2 - l2.r*cost1)/(sint1*cost2 - cost1*sint2 );
    		if( cost1 > cost2 )
    			x = l1.getX(y);
    		else
    			x = l2.getX(y);
    	}
    	if( x < -l1.centerX || x >= l1.centerX )
    		return null;
    	if( y < -l1.centerY || y >= l1.centerY )
    		return null;
    	
    	Point2D inter = new Point2D.Double();
    	inter.setLocation(x + l1.centerX, y + l1.centerY);
    	return inter;
    }
    
    // sort in the order of vote
	@Override
		public int compareTo(HoughLine that) {
		return (int) ( this.vote - that.vote );

	} 
    
} 