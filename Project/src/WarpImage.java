import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.util.ArrayList;

import javax.media.jai.Interpolation;
import javax.media.jai.JAI;
import javax.media.jai.PerspectiveTransform;
import javax.media.jai.PlanarImage;
import javax.media.jai.WarpPerspective;


// Perform perspective projection on images to wrap to another perspective

public class WarpImage {
	

	BufferedImage srcImage;
	BufferedImage cropped;
	BufferedImage warpped;
	BufferedImage updated;
	PerspectiveTransform trans;
	Point cropPosition;
	// warped image width & height
	int width;
	int height;
	
	public WarpImage(BufferedImage source)
	{
		srcImage = source;
		cropPosition = new Point();
	}

	// set the corners for warping to the expected rectangle of width and height
	// the corners should be specified in the order of upper left, upper right,
	// lower right, lower left.
	public void setTransformation(ArrayList<Point2D>corners, int outWidth, int outHeight)
	{
		double minX = srcImage.getWidth();
		double minY = srcImage.getHeight();
		double maxX = -1;
		double maxY = -1;
		width = outWidth;
		height = outHeight;
		
		// find the binding rectangle
		for(Point2D p: corners)
		{
			double x = p.getX();
			double y = p.getY();
			
			if( y < minY)
				minY = y;
			if( y > maxY)
				maxY = y;
			if( x < minX)
				minX = x;
			if( x > maxX)
				maxX = x;
		}
		// crop it out
		cropPosition.setLocation(minX,  minY);
		cropped = new BufferedImage((int)(maxX - minX), (int)(maxY - minY),  BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) cropped.getGraphics();
		g.drawImage(srcImage.getSubimage((int)minX, (int)minY, (int)(maxX - minX), (int)(maxY - minY) ), 0, 0, null);
		g.dispose();
		
		// generate the projection matrix 
		this.trans = PerspectiveTransform.getQuadToQuad(corners
				.get(0).getX() - minX, corners.get(0).getY() - minY, corners
				.get(1).getX() - minX, corners.get(1).getY() - minY, corners
				.get(2).getX() - minX, corners.get(2).getY() - minY, corners
				.get(3).getX() - minX, corners.get(3).getY() - minY, 0, 0,
				outWidth, 0, outWidth, outHeight, 0, outHeight);
				
	}
	
	// get the cropped, non-warped image
	public BufferedImage getCropped()
	{
		return cropped;
	}
	
	// get the warped image
	public BufferedImage getWarpped()
	{
		return warpped;
	}
	
	// get the restored back to original perspective, cropped image
	public BufferedImage getUpdated()
	{
		return updated;
	}

	// draw an image on the warped image
	public void makeMove(int x, int y, BufferedImage piece)
	{
		Graphics2D g = (Graphics2D) warpped.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.5f));
		g.drawImage(piece, x, y, null);
		g.dispose();
	}
	
	// restore back to orginal perspective and overlay back to original photo
	public void overLayToSource()
	{

		WarpPerspective warp = new WarpPerspective(trans);
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(PlanarImage.wrapRenderedImage(warpped));
		pb.add(warp);
		pb.add(Interpolation.getInstance(Interpolation.INTERP_BILINEAR));
		PlanarImage warpedImage = JAI.create("warp", pb);
		updated = warpedImage.getAsBufferedImage();
		srcImage.getGraphics().drawImage(updated, (int)cropPosition.getX(), (int)cropPosition.getY(), null);
	}
	
	// perform warp
	public void warp() {

		WarpPerspective warp = null;
		try {
			warp = new WarpPerspective(trans.createInverse());
		} catch (NoninvertibleTransformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(PlanarImage.wrapRenderedImage(cropped));

		pb.add(warp);
		pb.add(Interpolation.getInstance(Interpolation.INTERP_BILINEAR));
		PlanarImage warpedImage = JAI.create("warp", pb);

		if(width > 0 && height > 0)
			warpped = warpedImage.getAsBufferedImage(new Rectangle(0, 0, width, height), null);

	}
}
