import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.Point;
import java.awt.GridLayout;
import java.io.File;
import java.net.URL;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JSpinner;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;


import javax.swing.SpinnerNumberModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

@SuppressWarnings("serial")
public class ProjectUI extends JFrame {

	
	// the warped image size
	final static private int WARPPEDSIZE = 480;
	final static private double RADIUSLENGTH = (WARPPEDSIZE/9.5)/2;
	private JPopupMenu viewportPopup;
	private JLabel infoLabel = new JLabel("");
	private BufferedImage img;
	private BufferedImage cimg;
	private BufferedImage warpped;
	private BufferedImage whitepiece;
	private BufferedImage blackpiece;
	private Project imgProcessor;
	private WarpImage w;
	ArrayList<Point2D> corners = new ArrayList<Point2D>();
	ArrayList<Line2D> lines = new ArrayList<Line2D>();
	private Game gameData = new Game();
	private Thread streaming;

	public ProjectUI() {
		super("Group 7 Image Recognition of Othello");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JScrollPane scroller = new JScrollPane(new ImagePanel());
		this.add(scroller);
		this.add(infoLabel, BorderLayout.SOUTH);
		this.setSize(500, 500);
		this.setVisible(true);
	}

	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {}
		new ProjectUI();
	}

	private class ImagePanel extends JPanel implements MouseListener, ActionListener, MouseMotionListener {
		int row;
		int column;
		ProjectUIFunctions puif = new ProjectUIFunctions();
		
		public ImagePanel() {
			imgProcessor = new Project();
			this.addMouseListener(this);
			this.addMouseMotionListener(this);
		}

		public Dimension getPreferredSize() {
			int w = 0;
			int h = 0;
			if( img != null) {
				w += img.getWidth();
				h = img.getHeight();
			}
			if( cimg != null) {
				w += cimg.getWidth();
				if( cimg.getHeight() >  h)
					h = cimg.getHeight();
			}
			return (new Dimension(w, h));
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			if (cimg != null && img != null)
			{
				Graphics2D g2d = (Graphics2D)g;
				g2d.drawImage(cimg, 0, 0, this);
				g2d.drawImage(img, cimg.getWidth(), 0, this);
				renderCorners(g2d);
				for( int i=0;i<lines.size();i++)
				{
					g2d.setColor(Color.CYAN);
					g2d.draw(lines.get(i));
				}
				g2d.dispose();
			}
		}

		public void renderCorners(Graphics2D g2d) {
			double crossLength = 2.0;
			g2d.setColor(Color.RED);
			if (corners!=null) {
				for (Point2D p : corners) {
					Line2D l = new Line2D.Double(p.getX()-crossLength, p.getY()-crossLength, p.getX()+crossLength, p.getY()+crossLength);
					g2d.draw(l);
					l = new Line2D.Double(p.getX()-crossLength, p.getY()+crossLength, p.getX()+crossLength, p.getY()-crossLength);
					g2d.draw(l);
				}
			}
		}

		private void showPopup(MouseEvent e) {
			JPopupMenu.setDefaultLightWeightPopupEnabled(false);
			viewportPopup = new JPopupMenu();

			JMenuItem openImageMenuItem = new JMenuItem("Load any image ...");
			openImageMenuItem.addActionListener(this);
			openImageMenuItem.setActionCommand("open image");
			viewportPopup.add(openImageMenuItem);

			JMenuItem loadPerfectImageMenuItem = new JMenuItem("Load perfect image broad");
			loadPerfectImageMenuItem.addActionListener(this);
			loadPerfectImageMenuItem.setActionCommand("loadPerfect");
			viewportPopup.add(loadPerfectImageMenuItem);

			JMenuItem loadStramingImageMenuItem = new JMenuItem("Load Streaming Image");
			loadStramingImageMenuItem.addActionListener(this);
			loadStramingImageMenuItem.setActionCommand("loadStreamingImage");
			viewportPopup.add(loadStramingImageMenuItem);

			JMenuItem capImageMenuItem = new JMenuItem("Capture web cam Image");
			capImageMenuItem.addActionListener(this);
			capImageMenuItem.setActionCommand("capimage");
			viewportPopup.add(capImageMenuItem);
			
			JMenuItem loadcamMenuItem = new JMenuItem("Load camera image");
			loadcamMenuItem.addActionListener(this);
			loadcamMenuItem.setActionCommand("loadcam");
			viewportPopup.add(loadcamMenuItem);			

			JMenuItem load1ImageMenuItem = new JMenuItem("Load sample image 1");
			load1ImageMenuItem.addActionListener(this);
			load1ImageMenuItem.setActionCommand("load1");
			viewportPopup.add(load1ImageMenuItem);			

			JMenuItem load2ImageMenuItem = new JMenuItem("Load sample image 2");
			load2ImageMenuItem.addActionListener(this);
			load2ImageMenuItem.setActionCommand("load2");
			viewportPopup.add(load2ImageMenuItem);			

			
			JMenuItem reloadMenuItem = new JMenuItem("Reload current image");
			reloadMenuItem.addActionListener(this);
			reloadMenuItem.setActionCommand("reload");
			viewportPopup.add(reloadMenuItem);

			viewportPopup.addSeparator();

			JMenuItem equalizeItem = new JMenuItem("Equalize");
			equalizeItem.addActionListener(this);
			equalizeItem.setActionCommand("equalize");
			viewportPopup.add(equalizeItem);
			
			JMenuItem cannyItem = new JMenuItem("Canny edge");
			cannyItem.addActionListener(this);
			cannyItem.setActionCommand("canny");
			viewportPopup.add(cannyItem);

			JMenuItem blobItem = new JMenuItem("Locate Biggest Blob");
			blobItem.addActionListener(this);
			blobItem.setActionCommand("blob");
			viewportPopup.add(blobItem);

			JMenuItem houghEItem = new JMenuItem("Hough Transform using image");
			houghEItem.addActionListener(this);
			houghEItem.setActionCommand("houghEdge");
			viewportPopup.add(houghEItem);

			JMenuItem boundingEItem = new JMenuItem("Warp");
			boundingEItem.addActionListener(this);
			boundingEItem.setActionCommand("Warp");
			viewportPopup.add(boundingEItem);

			
			JMenuItem houghItem = new JMenuItem("Hough Circle");
			houghItem.addActionListener(this);
			houghItem.setActionCommand("houghcircle");
			viewportPopup.add(houghItem);


			JMenuItem drawPieceItem = new JMenuItem("Draw piece");
			drawPieceItem.addActionListener(this);
			drawPieceItem.setActionCommand("drawpiece");
			viewportPopup.add(drawPieceItem);

			
			viewportPopup.addSeparator();

			JMenuItem canny2Item = new JMenuItem("Canny in steps");
			canny2Item.addActionListener(this);
			canny2Item.setActionCommand("canny2");
			viewportPopup.add(canny2Item);			

			
			JMenuItem analyseItem = new JMenuItem("Analyse");
			analyseItem.addActionListener(this);
			analyseItem.setActionCommand("analyse");
			viewportPopup.add(analyseItem);
			
			JMenuItem exitMenuItem = new JMenuItem("Exit");
			exitMenuItem.addActionListener(this);
			exitMenuItem.setActionCommand("exit");
			viewportPopup.add(exitMenuItem);

			viewportPopup.show(e.getComponent(), e.getX(), e.getY());
		}

		public void mouseClicked(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}

		public void mousePressed(MouseEvent e) {
			if (viewportPopup != null) {
				viewportPopup.setVisible(false);
				viewportPopup = null;
			} else
				showPopup(e);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("open image")) {
				final JFileChooser fc = new JFileChooser();
				FileFilter imageFilter = new FileNameExtensionFilter("Image files", "bmp", "gif", "jpg");
				fc.addChoosableFileFilter(imageFilter);
				fc.setDragEnabled(true);
				fc.setMultiSelectionEnabled(false);
				int result =  fc.showOpenDialog(this);
				if (result == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						lines.clear();
						corners.clear();
						cimg = ImageIO.read(file);
						img = colorToGray(cimg);
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			} else if (e.getActionCommand().equals("loadPerfect")) {
				try {
					corners.clear();
					lines.clear();
					cimg = ImageIO.read(new URL("http://i.cs.hku.hk/~tyyau/perfect.png"));
					img = colorToGray(cimg);
					
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(this, "Unable to fetch image from URL", "Error",
							JOptionPane.ERROR_MESSAGE);
					ee.printStackTrace();
				}
			} else if (e.getActionCommand().equals("loadcam")) {
				try {
					corners.clear();
					lines.clear();
					cimg = ImageIO.read(new URL("http://i2.cs.hku.hk/~tyyau/7506/thumb_nails/test.jpg"));
					img = colorToGray(cimg);
					
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(this, "Unable to fetch image from URL", "Error",
							JOptionPane.ERROR_MESSAGE);
					ee.printStackTrace();
				}
			} else if (e.getActionCommand().equals("load1")) {
				try {
					corners.clear();
					lines.clear();
					cimg = ImageIO.read(new URL("http://i.cs.hku.hk/~tyyau/1.jpg"));
					img = colorToGray(cimg);
					
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(this, "Unable to fetch image from URL", "Error",
							JOptionPane.ERROR_MESSAGE);
					ee.printStackTrace();
				}	
			} else if (e.getActionCommand().equals("load2")) {
				try {
					corners.clear();
					lines.clear();
					cimg = ImageIO.read(new URL("http://i.cs.hku.hk/~tyyau/2.jpg"));
					img = colorToGray(cimg);
					
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(this, "Unable to fetch image from URL", "Error",
							JOptionPane.ERROR_MESSAGE);
					ee.printStackTrace();
				}					

			} else if (e.getActionCommand().equals("reload")) {
				if (cimg!= null) {
					img = colorToGray(cimg);
					lines.clear();
					corners.clear();
				}				
			} else if (e.getActionCommand().equals("gradientImage")) {
				if (img!= null) {
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					imgProcessor.gradientImage(imgData, img.getWidth(), img.getHeight());
				}
			} else if (e.getActionCommand().equals("equalize")) {
				if (img!= null) {
					img = colorToGray(img);
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					imgProcessor.histogramEqualization(imgData);
				}				
			} else if (e.getActionCommand().equals("thresholdTransformation")) {
				if (img!= null) {
					int threshold = 126;
					boolean notOk = true;
					while (notOk) {
						String s = JOptionPane.showInputDialog(this, "Please enter a threshold! \n(0 <= threshold <= 255)", ""+threshold);
						if (s==null) {
							return;
						}
						try {
							int i = Integer.parseInt(s);
							if (0<=i && i<=255) {
								notOk = false;
								threshold = i;
							}
						} catch (Exception ee){
						}
					}					
					img = colorToGray(img);
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					imgProcessor.thresholdTransformation(imgData, threshold);
				}
			} else if (e.getActionCommand().equals("medianImage")) {
				if (img!= null) {
					int threshold = 3;
					boolean notOk = true;					
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					while (notOk) {
						String s = JOptionPane.showInputDialog(null, "Please enter filter size)", ""+threshold);
						if (s==null) {
							return;
						}
						try {
							int i = Integer.parseInt(s);
							if (0<=i && i<=255 && i%2==1) {
								notOk = false;
								threshold = i;
							}
						} catch (Exception ee){
						}
					}					
					imgProcessor.medianFilter(imgData, img.getWidth(), img.getHeight(), threshold);
				}
			} else if (e.getActionCommand().equals("gaussianSmoothing")) {
				if (img!= null) {
					double sigma = 1.5;
					boolean notOk = true;
					while (notOk) {
						String s = JOptionPane.showInputDialog(this, "Please enter a sigma! \n(Must be positive)", ""+sigma);
						if (s==null) {
							return;
						}
						try {
							double d = Double.parseDouble(s);
							if (d>=0.0) {
								notOk = false;
								sigma = d;
							}
						} catch (Exception ee){
						}
					}
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					imgProcessor.gaussianSmoothing(imgData, sigma, img.getWidth(), img.getHeight());
				}
			} else if (e.getActionCommand().equals("cornerResponseImage")) {
				if (img!= null) {
					double sigma = 1.5;
					boolean notOk = true;
					while (notOk) {
						String s = JOptionPane.showInputDialog(this, "Please enter a sigma! \n(Must be positive)", ""+sigma);
						if (s==null) {
							return;
						}
						try {
							double d = Double.parseDouble(s);
							if (d>=0.0) {
								notOk = false;
								sigma = d;
							}
						} catch (Exception ee){
						}
					}
					double threshold = 10000000.0;
					notOk = true;
					while (notOk) {
						String s = JOptionPane.showInputDialog(this, "Please enter a threshold! \n(Must be positive)", ""+threshold);
						if (s==null) {
							return;
						}
						try {
							double d = Double.parseDouble(s);
							if (d>=0.0) {
								notOk = false;
								threshold = d;
							}
						} catch (Exception ee){
						}
					}
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					imgProcessor.cornerResponseImage(imgData, sigma, threshold, img.getWidth(), img.getHeight());
				}
			} else if (e.getActionCommand().equals("detectCorners")) {
				if (img != null) {
					img = colorToGray(img);
					new DetectCornersGUI().createAndShowGUI();
				}
			} else if (e.getActionCommand().equals("canny")) {
				if (img != null) {
					img = colorToGray(img);
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					long start = System.nanoTime();
					Canny canny = new Canny(imgData, img.getWidth(), img.getHeight());
					canny.process();
					img = canny.getMagnitude();
					double seconds = (System.nanoTime() - start) / 1000000000.0;
					infoLabel.setText(seconds+"");					
					
				}
			} else if (e.getActionCommand().equals("blob")) {
				if (img != null) {
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					imgProcessor.getBiggestBlob(imgData, img.getWidth(), img.getHeight());
//					PopUpPanel hpanel = new PopUpPanel(edgeimg);
//					hpanel.show();					
				}
			} else if (e.getActionCommand().equals("houghcircle")) {
				if (img != null) {
					gameData.initBoardData();
					puif.HoughTransformCirle();
				}
			} else if (e.getActionCommand().equals("canny2")) {
				if (img != null) {
					img = colorToGray(img);
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					Canny c = new Canny(imgData, img.getWidth(), img.getHeight());
					c.gradient();
					PopUpPanel panel = new PopUpPanel("Gradient", c.getGradient(false));
					panel.show(false);
					c.nonMaxSuppression16();
					PopUpPanel panel2 = new PopUpPanel("Non Maximum Suppression", c.getSuppressed());
					panel2.show(false);
					c.threshold();
					PopUpPanel panel4 = new PopUpPanel("Threshold", c.getMagnitude());
					panel4.show(false);
					
					c.hystersis();
					img = c.getMagnitude();
				}				
			} else if (e.getActionCommand().equals("houghEdge")) {
				if (img != null) {
					int numlines = 4;
					boolean notOk = true;
					while (notOk) {
						String s = JOptionPane.showInputDialog(this, "Please enter number of lines to detect", ""+numlines);
						if (s==null) {
							return;
						}
						try {
							int i = Integer.parseInt(s);
							if (i>0 && i < 100) {
								notOk = false;
								numlines = i;
							}
						} catch (Exception ee){
						}
					}					
					Hough h = new Hough(img.getWidth(), img.getHeight());
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					h.transform(imgData);
					double t = 0.9;
					Vector<HoughLine> v = new Vector<HoughLine> ();
					while( v.size() < numlines && t > 0 )
					{
						v = h.getLines(t);
						t -= 0.05;
					}
					if( v.size() < numlines )
					{
						JOptionPane.showMessageDialog(null, "Fail to detect the required lines", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						System.out.println("Lines are output: " + v.size() + " with threshold: " + t);
						for(HoughLine l: v)
						{
							System.out.println(l);
							lines.add(l.toLine2D());
						}
					}
				}
			} else if (e.getActionCommand().equals("Warp")) {
				if (img != null) {
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					corners = imgProcessor.setCorners(imgData, img.getWidth(), img.getHeight(), lines);
					if( corners!= null && corners.size() == 4 )
					{
						for(Point2D p: corners)
							System.out.println("C: " + p);
						w = new WarpImage(cimg);
						w.setTransformation(corners, WARPPEDSIZE, WARPPEDSIZE);
						w.warp();
						warpped = w.getWarpped();
						img = warpped;
					}
				}
			} else if ( e.getActionCommand().equals("capimage")) {
					CaptureUI cap = new CaptureUI(ProjectUI.this, "Capture");
					corners.clear();
					lines.clear();
					cimg = cap.showDialog();
					if(cimg != null)
						img = colorToGray(cimg);
					warpped = null;
					
			} else if (e.getActionCommand().equals("drawpiece")) {
				if (img != null) {
					int row=1;
					int col=1;
					boolean notOk = true;
					while (notOk) {
						String s = JOptionPane.showInputDialog("Please enter row & column (1-8), in the form of (row,column)", ""+row+ "," + col);
						if (s==null) {
							return;
						}
						try {
							String delims = "[, ]+";
							String[] tokens = s.split(delims);
							
							if(tokens.length>1)
							{
								row = Integer.parseInt(tokens[0]);
								col = Integer.parseInt(tokens[1]);
								if (row > 0 && row < 9 && col > 0 && col < 9) {
									notOk = false;
								}
							}
						} catch (Exception ee){
						}
					}			
					puif.makemove(Project.pieceColor.WHITE, row-1, col-1);
				}
			} else if (e.getActionCommand().equals("makemove")) {
				puif.makemove(Project.pieceColor.BLACK, 0, 0);
			} else if(e.getActionCommand().equals("loadStreamingImage")){
				if(streaming == null || !streaming.isAlive() || streaming.isInterrupted()){
					int option = -1;
	 				Object[] options = {"Cancel", "Black", "White"};
	 				while (option == -1) {
	 					option = JOptionPane.showOptionDialog(this, "Select a piece color for analyse", "Select a piece color for analyse", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
	 					if (option==0) {
	 						return;
	 					}
	 				}
	 				final int checkFor = option;
					streaming = new Thread(new Runnable(){
				            @Override
				            public void run() {
				            	try {
				                	while(true){
					        			puif.loadStreamimgImage();
					        			if(checkFor == 1){
					 						puif.Analyse(Project.pieceColor.BLACK);
					 					}else if(checkFor == 2){
					 						puif.Analyse(Project.pieceColor.WHITE);
					 					}
										callUpdateUI();
										Thread.sleep(10);
				                	}
								} catch (InterruptedException e) {
									System.out.println("Streaming stopped");
								}
				        	}
						});
					streaming.start();
				}else{
					streaming.interrupt();
				}
			} else if (e.getActionCommand().equals("analyse")) {
				int option = -1;
				Object[] options = {"Cancel", "Black", "White"};
				while (option == -1) {
					option = JOptionPane.showOptionDialog(this, "Select a piece color for analyse", "Select a piece color for analyse", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
					if (option==0) {
						return;
					}else if(option == 1){
						puif.Analyse(Project.pieceColor.BLACK);
					}else if(option == 2){
						puif.Analyse(Project.pieceColor.WHITE);
					}
				}
			} else if (e.getActionCommand().equals("exit")) {
				System.exit(0);
			}
			viewportPopup = null;
			this.updateUI();
		}
		
		public void callUpdateUI(){
			this.updateUI();
		}

		
		public class ProjectUIFunctions {
			public BufferedImage extractPiece(BufferedImage img, Point2D center, double radius)
			{
				BufferedImage piece = new BufferedImage((int)radius*2+1, (int)radius*2+1, BufferedImage.TYPE_4BYTE_ABGR);

				try{
					Graphics  g = piece.getGraphics();
					g.setClip(new Ellipse2D.Double(0, 0, radius*2+1, radius*2+1));
					g.drawImage(img.getSubimage( (int)(center.getX()-radius-0.5), (int)(center.getY() - radius-0.5), (int)radius*2+1, (int)radius*2+1), 0, 0, null);
					g.dispose();
				}catch( java.awt.image.RasterFormatException e){
					//exception
				}
				return piece;
			}
					
			public void loadStreamimgImage(){
				try {
					corners.clear();
					lines.clear();
					cimg = ImageIO.read(new URL("http://i.cs.hku.hk/~kychan/ip/uploaded_image.jpg?rand="+Math.random()));
					img = colorToGray(cimg);
					
				} catch (Exception ee) {
					ee.printStackTrace();
				}	
			}
			public void HoughTransformCirle(){
				if (img != null) {
					img = colorToGray(img);
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					Canny canny = new Canny(imgData, img.getWidth(), img.getHeight());
					canny.process();
					img = canny.getMagnitude();
					imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					System.out.println("Radius: " + RADIUSLENGTH);
					HoughCircle h = new HoughCircle(img.getWidth(), img.getHeight());
					// draw the circle with a bit tolerance
					h.transform(imgData, RADIUSLENGTH );
					h.transform(imgData, RADIUSLENGTH + 0.5 );
					h.transform(imgData, RADIUSLENGTH - 0.5 );
					PopUpPanel panel2 = new PopUpPanel("HoughCircle", h.getHoughGraph());
					panel2.show();
					List<Point2D> c = h.getCircles();
					if( c.size() > 0 )
					{
						Graphics2D g = (Graphics2D)warpped.getGraphics();
						g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
						g.setColor(Color.RED);
						g.setStroke(new BasicStroke(5));;
						System.out.println("Find " + c.size() + " circles");
						
						int isDraw[][] = new int[8][8];
						for( Point2D p: c)
						{
						    int sampleSize = (int)RADIUSLENGTH;
							int intensity = 0;
							int count = 0;
							for(int x = -sampleSize/2; x < sampleSize/2; x++){
								for(int y = -sampleSize/2; y < sampleSize/2; y++){
									try{
										
										Color color = new Color(warpped.getRGB((int)p.getX()+x, (int)p.getY()+y));
										intensity += (int)(color.getRed() * 0.2126 + color.getGreen() * 0.7152f + color.getBlue() * 0.0722f / 255);
										count++;
									}catch( java.lang.ArrayIndexOutOfBoundsException ex){
										//System.out.println("java.lang.ArrayIndexOutOfBoundsException");
									}
								}
							}
							intensity = intensity/count;
							System.out.println("Circle at: " + p+" of intensity "+ intensity);
							
							int gridsize = WARPPEDSIZE / 8;
							if( intensity > 100)
							{
								whitepiece =  puif.extractPiece(warpped, p, RADIUSLENGTH);
								gameData.setBoardData((int)p.getY()/ gridsize, (int)p.getX()/gridsize, 1);
								isDraw[(int)p.getY()/ gridsize][(int)p.getX()/gridsize] = 1;
							}
							else
							{
								blackpiece =  puif.extractPiece(warpped, p, RADIUSLENGTH);
								gameData.setBoardData((int)p.getY()/gridsize, (int)p.getX()/gridsize, -1);
								isDraw[(int)p.getY()/ gridsize][(int)p.getX()/gridsize] = 1;
							}
						    g.drawOval((int)(p.getX() - RADIUSLENGTH), (int)(p.getY() - RADIUSLENGTH), (int)(2*RADIUSLENGTH), (int)(2*RADIUSLENGTH));
						}
						g.dispose();
						img = warpped;
					}
				}
			}
			private boolean PerformWarp(byte[] buf, int width, int height)
			{
				Canny canny = new Canny(buf, width, height);
				canny.process();
				byte [] edge = canny.getMagnitudeArray();
				imgProcessor.getBiggestBlob(edge, width, height);
				
				ArrayList<Point2D> c = imgProcessor.setCorners(edge, width, height, lines);
				if( c == null || c.size() != 4)
				{
					return false;
				}	
				else
				{
					corners = c;
					for(Point2D p: corners)
						System.out.println("C: " + p);
					w = new WarpImage(cimg);
					w.setTransformation(corners, WARPPEDSIZE, WARPPEDSIZE);
					w.warp();
					warpped = w.getWarpped();
					img = warpped;
				}
				return true;
			}
			public void Warp() throws Exception {
				if( img != null)
				{
					byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					byte[] temp = Arrays.copyOf(imgData, imgData.length);
					// first try warp without equalize
					if( !PerformWarp(temp, img.getWidth(), img.getHeight()))
					{
						System.out.println("Equalize before warpping");
						temp = Arrays.copyOf(imgData, imgData.length);
						imgProcessor.histogramEqualization(temp);
						if(!PerformWarp(temp, img.getWidth(), img.getHeight())){
							//JOptionPane.showMessageDialog(null, "Fail to detect the game board", "Error", JOptionPane.ERROR_MESSAGE);
							System.out.println("Fail to detect the game board");
							throw new Exception();
						}		
					}
				}
			}
			private void drawPiece(BufferedImage image, BufferedImage gamepiece, int x, int y)
			{
				Graphics2D g = image.createGraphics();
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.3f));
				g.drawImage(gamepiece, x, y, null);
				g.dispose();
			}
			
			public void makemove(Project.pieceColor color, int row, int col) {
				if (img != null && warpped != null) {
					int offset = (int)(WARPPEDSIZE/8 - 2 * RADIUSLENGTH)/2;
					
					BufferedImage piece = color== Project.pieceColor.BLACK?blackpiece:whitepiece;
					drawPiece(img, piece,(int)WARPPEDSIZE/8 * col + offset, (int)WARPPEDSIZE/8 * row + offset);
					drawPiece(warpped, piece,(int)WARPPEDSIZE/8 * col + offset, (int)WARPPEDSIZE/8 * row + offset);
					w.overLayToSource();
				}				
			}
			public void Analyse(Project.pieceColor userCode){
				try{
					long start = System.nanoTime();
					puif.Warp();

					//step1
					BufferedImage colorImg = w.getWarpped();
					img = colorToGray(colorImg);
					
					gameData = new Game();
					gameData.initBoardData();
					
					puif.HoughTransformCirle();

					System.out.println("Current game state:");
					System.out.println(gameData);
					//step 5: find possible move
					for(int row = 0; row < 8; row++){
						for(int col = 0; col < 8; col++){
							int user = 1;
							if(userCode == Project.pieceColor.BLACK)
								user = -1;
							if(gameData.isPossibleMove(user, row, col)){
								//System.out.println("r"+row+",c"+col);
								puif.makemove(userCode, row, col);
							}
						}
					}
					double seconds = (System.nanoTime() - start) / 1000000000.0;
					infoLabel.setText(seconds+"");
					
				}catch(Exception ex){
					System.out.println("Fail to detect the game board");
					return;
				}
			}
		}
		
		public BufferedImage colorToGray(BufferedImage source) {
	        BufferedImage returnValue = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
	        Graphics g = returnValue.getGraphics();
	        g.drawImage(source, 0, 0, null);
	        return returnValue;
	    }

		public void mouseDragged(MouseEvent e) {}

		public void mouseMoved(MouseEvent e) {
			int intensity = 0;
			column = e.getX();
			row = e.getY();
			String rgb = "";
			if( cimg != null && column < cimg.getWidth() && row < cimg.getHeight())
				rgb = new Color(cimg.getRGB(column, row)).toString();
			if( cimg != null && img!=null &&  column >= cimg.getWidth() && column < img.getWidth() + cimg.getWidth() && row < img.getHeight())
			{
				column = column - cimg.getWidth();
				byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
				intensity = 0xFF & (int)imgData[row * img.getWidth()+ column] ;
				rgb = new Color(img.getRGB(column, row)).toString();
			}

			infoLabel.setText("("+column+","+row+") value(" + intensity + ") & rgb="+rgb);
		}
	}

	class DetectCornersGUI extends JPanel implements ActionListener {
	    JButton okButton;
	    JButton applyButton;
		JDialog dialog = null;
		JSpinner sigmaSpinner;
		JSpinner thresholdSpinner;
		public DetectCornersGUI() {
			super(new GridLayout(0,1));
	        okButton = new JButton("Ok");
	        okButton.setActionCommand("ok");
	        okButton.addActionListener(this);
	        applyButton = new JButton("Apply");
	        applyButton.setActionCommand("apply");
	        applyButton.addActionListener(this);
	        JPanel rootPanel = new JPanel();
	        GridLayout gridLayout = new GridLayout(2,1, 3, 3);
	        JPanel detectCornersPanel = new JPanel(gridLayout);
	        JLabel sigmaLabel = new JLabel("Sigma: ");
	        double valueSigma = 1.5;
	        double minSigma = 0.01;
	        double maxSigma = 5.0;
	        double stepSigma = 0.25;
	        SpinnerNumberModel sigmaSpinnerNumberModel = new SpinnerNumberModel( valueSigma, minSigma, maxSigma, stepSigma);
	        sigmaSpinner = new JSpinner( sigmaSpinnerNumberModel );
	        detectCornersPanel.add(sigmaLabel);
	        detectCornersPanel.add(sigmaSpinner);
	        JLabel thresholdLabel = new JLabel("Threshold: ");
	        double valueThreshold = 100000.0;
	        double minThreshold = -100000000;
	        double maxThreshold = 100000000.0;
	        double stepThreshold = 100000;
	        SpinnerNumberModel thresholdSpinnerNumberModel = new SpinnerNumberModel( valueThreshold, minThreshold, maxThreshold, stepThreshold );
	        thresholdSpinner = new JSpinner( thresholdSpinnerNumberModel );
	        detectCornersPanel.add(thresholdLabel);
	        detectCornersPanel.add(thresholdSpinner);
			rootPanel.add(detectCornersPanel);
	        JPanel panel = new JPanel();
	        panel.add(applyButton);
	        rootPanel.add(panel);
	        add(rootPanel);
	        GUIValuesToProgram();
		}
		
	    public void createAndShowGUI() {
	        dialog = new JDialog(ProjectUI.this, "Corner Detection", false);
	        dialog.setLocation(new Point(ProjectUI.this.getLocation().x+100, ProjectUI.this.getLocation().y+100));
	        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	        dialog.setModal(true);
	        dialog.setContentPane(this);
	        dialog.pack();
	        dialog.setResizable(false);
	        dialog.setVisible(true);
	    }
	    public void GUIValuesToProgram() {
			double sigma = (Double)sigmaSpinner.getValue();
			double threshold = (Double)thresholdSpinner.getValue();
			byte[] imgData = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
			corners = imgProcessor.detectCorners(imgData, img.getWidth(), img.getHeight(), sigma, threshold);
			ProjectUI.this.repaint();
	    }
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand() == "apply") {
				GUIValuesToProgram();
				repaint();
			}
		}
	}
	private class PopUpPanel extends JPanel {
		private BufferedImage himg;
		private JDialog dialog = null;
		private String title;
		
		public PopUpPanel(String title, BufferedImage img) {
			himg = img;
			this.title  = title;
		}

		public void show(boolean modal)
        {
			dialog = new JDialog(ProjectUI.this, title, false);
	        dialog.setLocation(new Point(ProjectUI.this.getLocation().x+100, ProjectUI.this.getLocation().y+100));
	        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	        dialog.setModal(modal);
			JScrollPane scroller = new JScrollPane(this);
			dialog.add(scroller);
	        dialog.setContentPane(scroller);
	        dialog.pack();
	        dialog.setResizable(false);
	        dialog.setVisible(true);
        }

		public void show()
        {
			show(false);
        }
		
		public Dimension getPreferredSize() {
			if (himg != null) return (new Dimension(himg.getWidth(), himg.getHeight()));
			else return (new Dimension(0, 0));
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			if (himg != null)
			{
				g.drawImage(himg, 0, 0, this);
			}
		}
	}
}

