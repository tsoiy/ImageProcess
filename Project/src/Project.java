import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;


public class Project {
	
	public enum pieceColor { BLACK, WHITE};
	

	/**
	 * Task 1:
	 * Display the gradient image.
	 * To determine the derivative in y direction use the mask [-1 0 1]. 
	 * Transpose the mask to determine the derivative in x direction.
	 *
	 * @param f the graylevel image (row major representation)
	 * @param width of the image
	 * @param height of the image
	 */
	public void gradientImage(byte[] f, int width, int height) {
		byte origImage[] = Arrays.copyOf(f, f.length);
		double c = 255.0 / Math.sqrt(255*255*2);
		for(int x=1;x<height-1;x++)	{
			for(int y=1;y<width-1;y++)	{
				double fx = (origImage[(x+1)*width+y] & 0xFF) - (origImage[(x-1)*width+y] & 0xFF);
				double fy = (origImage[x*width+y+1] & 0xFF) - (origImage[x*width+y-1] & 0xFF);
				f[x*width+y] = (byte)(c * Math.sqrt(fx*fx + fy*fy));
			}
		}
	}

	/**
	 * Task 2:
	 * Implement 2D Gaussian smoothing.
	 * For ease of implementation you may ignore the boundary case.
     * Perform the 2D filtering by applying 1D filters. 
     * Compute and use a proper filter size for a 1D Gaussian mask based on the sigma parameter. 
	 *
	 * @param f the graylevel image (row major representation)
	 * @param threshold for the response map display
	 * @param width of the image
	 * @param height of the image
	 */
	public void gaussianSmoothing(byte[] f, double sigma, int width, int height) {
		double fDouble[] = new double[f.length];
		for(int i =0;i < f.length;i++)
			fDouble[i] = f[i] & 0xFF;
		smoothing(fDouble, sigma, width, height);
		for(int i=0;i<f.length;i++)
			f[i] = (byte)fDouble[i];
	}
	
	private void smoothing(double[] f, double sigma, int width, int height){
		double gaussianDieOff = 0.001;
		ArrayList<Double>halfMask = new ArrayList<Double>();
		for(int i =0;true;i++) {
			double d = Math.exp(-i*i/(2.0*sigma*sigma));
			if(d<gaussianDieOff) break;
			halfMask.add(d);
		}
		ArrayList<Double> fullMask = new ArrayList<Double>(halfMask);
		Collections.reverse(fullMask);
		fullMask.addAll(halfMask.subList(1,  fullMask.size()));
		double sumFullMask = fullMask.stream().mapToDouble(Double::doubleValue).sum();
//		for(double d :fullMask)
//			System.out.println(d);
		for(int i=0;i<fullMask.size();i++)
			fullMask.set(i, fullMask.get(i)/sumFullMask);
		double[] fullMaskArray = fullMask.stream().mapToDouble(Double::doubleValue).toArray();
		convolve(f, width, height, fullMaskArray, fullMask.size()/2, 0);
		convolve(f, width, height, fullMaskArray, 0, fullMask.size()/2);
	}

	private void convolve(double[] f, int width, int height, double w[], int a, int b) {
		double [] f2 = new double[f.length];
		int filterSize = Math.min(2*a + 1, 2*b + 1);
		System.arraycopy(f,  0,  f2,  0,  f.length);
		for(int x=a; x<height-a;x++) {
			for(int y = b;y<width-b;y++) {
				double sum=0;
				for(int s=-a;s<=a;s++)
					for(int t=-b;t<= b;t++)
						sum+= w[(s+a)*filterSize + t +b ]*(f2[(x+s)*width+y+t]);
				f[x*width+y] = sum;
			}
		}
	}
	
	public int[] histogram (byte[] img) {
		//System.out.println("TODO: Task 5");
		int[] returnValue = new int[256];
		for (byte b :img) {
			returnValue[b & 0xFF]++;
		}
		return returnValue;
	}

	/**
	 * Task 6:
	 * Calculate the cumulative histogram of the image.
	 * @param img
	 * @return the histogram
	 */
	public int[] cumulativeHistogram (byte[] img) {
		//System.out.println("TODO: Task 6");
		int[] histogram = histogram(img);
		int sum = histogram[0];
		for(int i=1;i<histogram.length;i++)
		{
			sum += histogram[i];
			histogram[i]= sum;
		}
		return histogram;
	}

	/**
	 * Task 7:
	 * Perform histogram equalization.
	 * @param img
	 */
	public void histogramEqualization(byte img[]) {
        int[] cum = cumulativeHistogram(img);
        for(int i=0;i<img.length;i++)
        {
        	img[i] = (byte)((255.0/img.length) * cum[img[i]&0xFF]);
        }
	}

    public void thresholdTransformation(byte[] img, int threshold) {
    	for (int i = 0; i < img.length; i++) {
    		if (((int)img[i] & 0xFF) < threshold )
    			img[i] = 0;
    		else
    			img[i] = (byte)0xFF;

    	}
    }
    
	public void medianFilter(byte[] img, int w, int h, int filterSize) {
		byte[] newimg = new byte[img.length];
		byte[] median = new byte[filterSize * filterSize];
		System.arraycopy(img, 0, newimg, 0, img.length);
		int a = filterSize/2;
		int x1;
		int y1;
		int count;
		for(int x=0;x<h;x++)
			for(int y=0;y<w;y++)
			{
				count=0;
				for(int s=-a;s<=a;s++)
					for(int t=-a;t<=a;t++)
					{
						x1 = x+s;
						y1 = y+t;
						if(x1 < 0 || x1 > h-1 || y1 < 0 || y1 > w - 1)
							continue;
						median[count++] = newimg[x1*w+y1];
					}
				Arrays.sort(median);
				img[x*w+y] = median[count/2];
			}
	}
	
	private double[] getR(byte[] f, double sigma, int width, int height) {
		double fx2[] = new double[f.length];
		double fy2[] = new double[f.length];
		double fxy[] = new double[f.length];
		double R[]   = new double[f.length];
		for(int x = 1;x<height-1;x++) {
			for( int y = 1;y<width-1;y++) {
				int fx = ((int)f[(x+1)*width+y] & 0xFF) - ((int)f[(x-1)*width+y] & 0xFF);
				int fy = ((int)f[x*width+y+1] & 0xFF) - ((int)f[x*width+y-1] & 0xFF);
				fx2[x*width+y] = fx*fx;
				fy2[x*width+y] = fy*fy;
				fxy[x*width+y] = fx*fy;
			}
		}
		smoothing(fx2, sigma, width, height);
		smoothing(fy2, sigma, width, height);
		smoothing(fxy, sigma, width, height);
		for(int x=1;x<height-1;x++)
			for(int y=1;y<width-1;y++)
				R[x*width+y] = fx2[x*width+y]*fy2[x*width+y] 
							  - fxy[x*width+y] * fxy[x*width+y] 
							  - 0.04*((fx2[x*width+y]+fy2[x*width+y])*(fx2[x*width+y]+fy2[x*width+y]));
		return R;
	}
    /**
	 * Task 3:
	 * Display the response map R of the Harris corner detector.
	 * For ease of implementation you may ignore the boundary case.
	 * Set pixels to white if R > threshold otherwise set it to black.
	 *
	 * @param f the graylevel image (row major representation)
	 * @param threshold for the response map display
	 * @param width of the image
	 * @param height of the image
	 */
	public void cornerResponseImage(byte[] f, double sigma, double threshold, int width, int height) {
		double R[] = getR(f, sigma, width, height);
		for(int i=0;i<R.length;i++)
			if(R[i] > threshold ) f[i] = (byte)255;
			else f[i] = (byte)0;
	}

	/**
	 * Task 4: 
	 * Implement the Harris Corner Detection Algorithm.
	 * For ease of implementation you may ignore boundary case.
	 * Perform non-maximal suppression and computer corners up to sub-pixel accuracy.
	 *
	 * @param f the graylevel image (row major representation)
	 * @param width of the image
	 * @param height of the image
	 * @param sigma used for smoothing
	 * @param threshold used to detect corners
	 */
	public ArrayList<Point2D> detectCorners(final byte[] f, int width, int height, 
											 double sigma, double threshold) {
		double R[] = getR(f, sigma, width, height);
		ArrayList<Point2D> cornersOut = new ArrayList<Point2D>();
		for(int y = 1;y<height-1;y++) {
			for(int x=1;x<width-1;x++) {
				int iwj = y*width+x;
				double val = R[iwj];
				if( val>threshold && val > R[iwj-width-1] && val > R[iwj - width]
				   && val > R[iwj - width + 1] && val > R[iwj-1]
				   && val > R[iwj + 1] && val > R[iwj + width-1]
				   && val > R[iwj + width] && val > R[iwj + width + 1]) {
					double cornerX = x + 0.5 + (R[iwj-1]-R[iwj+1])/
							(R[iwj -1] + R[iwj+1]-2*val)/2;
					double cornerY = y + 0.5 + (R[iwj-width] - R[iwj+width])/
							(R[iwj-width] + R[iwj+width] -2 * val)/2;
					cornersOut.add(new Point2D.Double(cornerX, cornerY));
				}
			}
		}
		//cornersOut.add(new double[]{10, 100.0});
//		System.out.println("No of corners: " + cornersOut.size());
//		filterCorners(cornersOut, 10);
//		System.out.println("No of corners: " + cornersOut.size());
		return cornersOut;
	}
	

	private int floodFill(byte[] f, int x, int y, byte matchColor, byte fillColor, int width, int height)
	{
		int size = 0;
		int[] curPoint = {x, y};
		int[] newPoint;
		boolean[][] process = new boolean[height][width];
		
		Queue<int[]>toBeFlooded = new LinkedList<int[]>();
		toBeFlooded.add(curPoint);
		process[x][y] = true;
		while( toBeFlooded.size() > 0 )
		{
			curPoint = toBeFlooded.remove();
			if( f[curPoint[0]*width + curPoint[1]] == matchColor)
			{
				f[curPoint[0]*width + curPoint[1]] = fillColor;
				size++;
			}
			for(int i=-1; i<2;i++)
				for(int j=-1;j<2;j++)
				{
					if(i==0 && j==0) continue;
					if( curPoint[0]+i < 0 || curPoint[0] + i >= height ) continue;
					if( curPoint[1]+j < 0 || curPoint[1] + j >= width ) continue;
					if( f[(curPoint[0]+i)*width+curPoint[1]+j] == matchColor )
					{
						if(!process[curPoint[0]+i][curPoint[1]+j])
						{
							process[curPoint[0]+i][curPoint[1]+j] = true;
							newPoint = new int[2];
							newPoint[0] = curPoint[0]+i;
							newPoint[1] = curPoint[1]+j;
							toBeFlooded.add(newPoint);
						}
					}
				}
		}
		return size;
	}
	public void getBiggestBlob(byte[] f, int width, int height) {
		byte[]temp = Arrays.copyOf(f, f.length);
		int maxsize = -1;
		int loc = 0;
		int size;
		for(int x=0;x< height; x++)
		{
			for(int y = 0; y < width; y++ )
			{
				if( temp[x*width+y] == (byte)255)
				{
					size = floodFill(temp, x, y, (byte)255, (byte)127, width, height);
					if( size > maxsize )
					{
						maxsize = size;
						loc = x*width+y;
					}
				}
			}
		}
		for(int x = 0;x < height; x++)
			for(int y = 0; y< width; y++)
				if(f[x*width+y]== (byte)255)
					f[x*width+y] = (byte)10;
		floodFill(f, loc/width, loc%width, (byte)10, (byte)255, width, height);
		thresholdTransformation(f, 128);
	}
	
	
	public class CornerComparator implements Comparator<Point2D> {
		
		Point2D base;
		
		public CornerComparator(Point2D base) {
			this.base = base;
		}
		public int compare(Point2D p1, Point2D p2) {
		
			double t1 = Math.atan2(p1.getY() - base.getY(), p1.getX()- base.getX()) + Math.PI;
			double t2 = Math.atan2(p2.getY() - base.getY(), p2.getX()- base.getX()) + Math.PI;
			
			return((int)(t1 - t2));
		}
	}	
	
	public ArrayList<Point2D> setCorners(byte[]img, int width, int height, ArrayList<Line2D> lines)
	{
		int minX = height;
		int minY = width;
		int maxX = -1;
		int maxY = -1;
		double edgeBuffer = 0.03;
		ArrayList<Point2D> corners = new ArrayList<Point2D>();
		
		// find the bounding rectangle containing the blob
		for(int x=0;x < height;x++)
			for(int y=0;y < width; y++){
				if( ((int)img[x*width+y] & 0xFF) == 255 )
				{
					if( y < minY)
						minY = y;
					if( y > maxX)
						maxX = y;
					if( x < minX)
						minX = x;
					if( x > maxY)
						maxY = x;
				}
			}
		// extend the rectangle a bit as the fitting line may extend out of it
		if( minY - width * edgeBuffer > 0 )
			minY = minY - (int)(edgeBuffer * width);
		else minY = 0;
		if( minX - height * edgeBuffer > 0 )
			minX = minX - (int)(edgeBuffer * height);
		else minX = 0;
		
		if( maxX + width * edgeBuffer > width )
			maxX = maxX + (int)(edgeBuffer * width );
		else maxX = width;
		if( maxY + height * edgeBuffer > 0 )
			maxY = maxY + (int)(edgeBuffer * height);
		else minX = height;
		
		Hough h = new Hough(width, height);
		h.transform(img);
		double t = 0.9;
		Vector<HoughLine> v = new Vector<HoughLine> ();
		while( v.size() < 4 && t > 0)
		{
			v = h.getLines(t);
			t -= 0.05;
		}
		for(int i=0;i<v.size()-1;i++)
			for(int j=i+1;j<v.size();j++)
			{
				Point2D c = HoughLine.getLineIntersection(v.get(i),  v.get(j));
				if( c != null )
				{
					if(c.getX() <= maxX && c.getX() >= minY && c.getY() <= maxY && c.getY() >= minX)
						corners.add(c);
				}
			}
		
		if( corners.size() < 4 )
			return null;
		
		// find the center of the
		double centX = 0, centY = 0;
		
		for(Point2D p: corners)
		{
			centX += p.getX();
			centY += p.getY();
		}
		centX /= corners.size();
		centY /= corners.size();
		
		// sort the corners in clockwise direction to set the correct order for
		// perspective projection
		
		if(lines != null)
		{
			lines.clear();
			for(HoughLine hl: v)
				lines.add(hl.toLine2D());
		}
		corners.sort(new CornerComparator(new Point2D.Double(centX, centY)));
		return corners;
	}
	
	public void Highlight(byte[] img, int l, int u){
		for(int i = 0; i < img.length; i++){
			if((img[i] & 0xFF) < l){
				img[i] = (byte)(img[i] & 0xFF / 2 );
			}else if((img[i] & 0xFF) > u){
				img[i] = (byte)(255 - (255 - img[i] & 0xFF)/ 2 );
			}else{
				double slope = (255/2+u/2-l/2)/(u-l);
				img[i] = (byte)(l/2 + (int)((img[i] & 0xFF - l) * slope));
			}
		}
	}
	
	public void Highlight2(byte[] img, int idx){
		for(int i = 0; i < img.length; i++){
			img[i] = (byte)((Math.pow((img[i] & 0xFF-128)/128.0, idx)+1)/2*255);
		}
	}
	
}

